![IMG_20150315_203158.png](https://bitbucket.org/repo/KeyXA5/images/2933312600-IMG_20150315_203158.png)
![2.png](https://bitbucket.org/repo/KeyXA5/images/1070586493-2.png)
![buttons.jpg](https://bitbucket.org/repo/KeyXA5/images/1258642115-buttons.jpg)

Screenshots taken from the example prj. For the background has been used a MBcontainer with a radial gradient. The button changes when the controls change.


First of all make sure to add all these lines in your cpp before QmlDocument::create([...]) :
```
#!c++
qmlRegisterType<MBcontainer>("MBcontainer", 1, 0, "MBcontainer");
qmlRegisterType<GradientStop>("MBcontainer", 1, 0, "GradientStop");
qmlRegisterUncreatableType<GradientKind>("MBcontainer", 1, 0, "GradientKind", "");
qmlRegisterUncreatableType<ShapeKind>("MBcontainer", 1, 0, "ShapeKind", "");

qmlRegisterType<MBbutton>("MBbutton", 1, 0, "MBbutton");
```
Then simply add this to your QMLs:

```
#!c++

import MBcontainer 1.0
import MBbutton 1.0
```




**MBbutton** is derived from *MBcontainer*
==========
This Container defines its own DockLayout so it's not recommended to define one in QML

**PROPERTIES**

*clickSound*: plays the key input keypress system sound (true by default)

*stateUp* and *stateDown* properties define a MBcontainer for the unpressed and pressed states of the button

 QML example:
 
```
#!java

MBbutton {
      id: mbutton
      clickSound: true
      onClicked: {
          console.log("Clicked")
      }
      stateUp: MBcontainer {
                  shapeType: ShapeKind.TypeRoundedRect
                  roundPerc: 0.3
                  fillerColor: Color.create(0, 0, 0.8, 0.6)
                  outlineColor: Color.create(0.5, 0.5, 0.5, 1)
                  [...]
      }
      // if you want the same aspect as the unpressed button for the pressed state,
      // reference the properties you would like to do not change to the stateUp and
      // set the property that differs.
      // In this example the fillerColor will change when pressed
      stateDown: MBcontainer {
                  shapeType: mbutton.stateUp.shapeType
                  roundPerc: mbutton.stateUp.roundPerc
                  fillerColor: Color.create(0, 0, 0.3, 0.6)
                  outlineColor: mbutton.stateUp.outlineColor
                  [...]
      }
}
```

**MBcontainer** is a custom control that can be used as a normal Container.
================================

It can have its custom color. The color can be a flat color, a linear, radial or conical gradient.

Gradients can have as many stops as needed.

MBcontainer can have a border with thickness and its own color.

Different shapes are available. The default is TypeRoundedRect which can assube shapes from a rectangle

to a circle/ellipses depending on the roundPerc property.

An image can be drawn inscribed into the shape.

Look here to understand how gradients work: http://qt-project.org/doc/qt-4.8/qgradient.html

**PROPERTIES**

*layoutHandler*: this property is created at runtime and can be used as the LayoutUpdateHandler

*shapeType*: possible values: 

* ShapeKind.TypeRoundedRect (default)
* ShapeKind.TypeBottomEdge
* ShapeKind.TypeTopEdge
* ShapeKind.TypeLeftEdge
* ShapeKind.TypeRightEdge
* ShapeKind.TypeSickleDown
* ShapeKind.TypeSickleUp
* ShapeKind.TypeSickleRight
* ShapeKind.TypeSickleLeft


*roundPerc*: expressed in 0-1 percentage. 0 menas hard edges, 1 means full rounded. Calculated on the minor edge.

*borderWidth*: border thickness expressed in pixels

*outlineColor*: border color (ex: Color.Yellow)

*fillerColor*: filler color when the GradientKind.TypeNone is used

*gradientStops*: GradientStop kind property. Is possible to define as many stops are needed. Example:

```
#!c++
gradientStops: [
         GradientStop { position: 1.0 ; color: Color.Blue } ,
         GradientStop { position: 0.6 ; color: Color.Green } ,
         GradientStop { position: 0.2 ; color: Color.Cyan } ,
         GradientStop { position: 0.0 ; color: Color.Yellow }
]
```


*gradientType*: possible values:

* GradientKind.TypeNone (default)
* GradientKind.TypeLinear
* GradientKind.TypeRadial
* GradientKind.TypeConical

*Properties used when the TypeLinear gradient is used. Values are expressed in 0-1 percentage ie top right corner has (0,0) coords, bottom right corner has (1,1)*

*gradientStartX*: x start position for the linear gradient expressed in 0-1 percentage

*gradientStartY*: y start position for the linear gradient expressed in 0-1 percentage

*gradientStopX*:  x stop position for the linear gradient expressed in 0-1 percentage

*gradientStopY*:  y stop position for the linear gradient expressed in 0-1 percentage


*Properties used when the TypeRadial gradient is used*

*gradientRadialCenterX*: x center position for the radial gradient expressed in 0-1 percentage

*gradientRadialCenterY*; y center position for the radial gradient expressed in 0-1 percentage

*gradientRadialRadius*: radius for the radial gradient expressed in 0-1 percentage (1.0 is the half of the minor edge)


*Properties when the TypeConical gradient is used*

*gradientConicalAngle*: starting angle for the conical gradient brush. The angle must be specified in degrees between 0 and 360



*imageSource*: local image file name

*scalingMethod*: possible values:

* ScalingMethod.Fill
* ScalingMethod.AspectFit (default)
* ScalingMethod.AspectFill

*url*: remote image url
*imageScaleX*: width scale. Must be >= 0 and 1 means no zoom
*imageScaleY*: height scale. Must be >= 0 and 1 means no zoom

**MIT License**

Copyright (c) 2015 Marco Bavagnoli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
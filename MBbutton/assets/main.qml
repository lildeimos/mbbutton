import bb.cascades 1.0
import MBcontainer 1.0
import MBbutton 1.0

Page {
    Container {
        layout: AbsoluteLayout {}
         attachedObjects: [
             LayoutUpdateHandler {
                 id: pageLUH
             }
         ]
        Container {
            layout: DockLayout {}
            /////////////////////////////////
            // BACKGROUND
            MBcontainer {
                preferredWidth: pageLUH.layoutFrame.width
                preferredHeight: pageLUH.layoutFrame.height
                shapeType: ShapeKind.TypeRoundedRect
                roundPerc: 0
                borderWidth: 0
                gradientType: GradientKind.TypeRadial
                // Gradient stops for all kind of gradients
                gradientStops: [  
                    GradientStop { position: 1.0 ; color: Color.Blue } ,
                    GradientStop { position: 0.8 ; color: Color.Red } , 
                    GradientStop { position: 0.6 ; color: Color.Green } ,
                    GradientStop { position: 0.45; color: Color.Black } ,
                    GradientStop { position: 0.3 ; color: Color.White } ,
                    GradientStop { position: 0.2 ; color: Color.Cyan } , 
                    GradientStop { position: 0.0 ; color: Color.Yellow } 
                ]
                
                // Top left corner for TypeLinear
                gradientStartX: 0.2
                gradientStartY: 0
                // Bottom right corner for TypeLinear
                gradientStopX: 1
                gradientStopY: 0.8
                // center the radial gradient with a radius that is a half of the minor edge. For TypeRadial
                gradientRadialCenterX: 0.7
                gradientRadialCenterY: 0.5
                gradientRadialRadius: 1
            }
            
            
            
            /////////////////////////////
            /////////////////////////////
            // MBBUTTON PARAMETERS
            Container {
                topPadding: 20
                
                Container {
                    bottomPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Button {
                        text: "None"
                        onClicked: mbutton.stateUp.gradientType = GradientKind.TypeNone
                    }
                    Button {
                        text: "Linear"
                        onClicked: mbutton.stateUp.gradientType = GradientKind.TypeLinear
                    }
                }
                Container {
                    bottomPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Button {
                        text: "Radial"
                        onClicked: mbutton.stateUp.gradientType = GradientKind.TypeRadial
                    }
                    Button {
                        text: "Conical"
                        onClicked: mbutton.stateUp.gradientType = GradientKind.TypeConical
                    }
                }
                Container {
                    bottomPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Button {
                        text: "1"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeRoundedRect
                    }
                    Button {
                        text: "2"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeBottomEdge
                    }
                    Button {
                        text: "3"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeTopEdge
                    }
                    Button {
                        text: "4"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeLeftEdge
                    }
                    Button {
                        text: "5"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeRightEdge
                    }
                }
                Container {
                    bottomPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Button {
                        text: "6"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeSickleDown
                    }
                    Button {
                        text: "7"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeSickleUp
                    }
                    Button {
                        text: "8"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeSickleRight
                    }
                    Button {
                        text: "9"
                        onClicked: mbutton.stateUp.shapeType = ShapeKind.TypeSickleLeft
                    }
                }
                Container {
                    bottomPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    ToggleButton {
                        id: imgToggle
                        onCheckedChanged: {
                            if (checked) {
                                mbutton.stateUp.imageSource = "";
//                                mbutton.stateUp.imageSource = "asset:///images/lama.jpg"
                                mbutton.stateUp.url = "http://store.shopblackberry.com/Storefront/Company/bbrry/images/detail/ProductMainImages/PRD-60828-PassportRed/Passport_Red_Front.png"
                            }
                            else {
                                mbutton.stateUp.imageSource = "";
                                mbutton.stateUp.url = ""
                            } 
                        }
                    }
                    Button {
                        text: "AspectFit"
                        enabled: imgToggle.checked
                        onClicked: mbutton.stateUp.scalingMethod = ScalingMethod.AspectFit
                    }
                    Button {
                        text: "AspectFill"
                        enabled: imgToggle.checked
                        onClicked: mbutton.stateUp.scalingMethod = ScalingMethod.AspectFill
                    }
                    Button {
                        text: "Fill"
                        enabled: imgToggle.checked
                        onClicked: mbutton.stateUp.scalingMethod = ScalingMethod.Fill
                    }
                }
                // WIDTH
                Container {
                    topPadding: 50
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Slider {
                        maxWidth: pageLUH.layoutFrame.width>>1
                        fromValue: 10.0
                        toValue: 700.0
                        value: 700
                        onValueChanged: {
                            mbutton.stateUp.preferredWidth = value
                        }
                    }
                    Label { text: "preferrdWidth" }
                }
                
                // HEIGTH
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Slider {
                        maxWidth: pageLUH.layoutFrame.width>>1
                        fromValue: 10.0
                        toValue: 700.0
                        value: 300
                        onValueChanged: {
                            mbutton.stateUp.preferredHeight = value
                        }
                    }
                    Label { text: "preferrdHeight" }
                }
                
                // THICKNESS
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Slider {
                        id: sliderThickness
                        maxWidth: pageLUH.layoutFrame.width>>1
                        fromValue: 0.0
                        toValue: 100.0
                        value: mbutton.stateUp.borderWidth
                        onValueChanged: {
                            mbutton.stateUp.borderWidth = value
                        }
                    }
                    Label { text: "borderWidth" }
                }
                
                // ROUNDPERC
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Slider {    
                        maxWidth: pageLUH.layoutFrame.width>>1
                        fromValue: 0.0
                        toValue: 4.0
                        value: 0.5
                        onValueChanged: {
                            mbutton.stateUp.roundPerc = value
                        }
                    }
                    Label { text: "roundPercs" }
                }
                
                // ANGLE for conical gradient
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    visible: mbutton.stateUp.gradientType == GradientKind.TypeConical
                    Slider {    
                        maxWidth: pageLUH.layoutFrame.width>>1
                        fromValue: 0.0
                        toValue: 360.0
                        value: mbutton.stateUp.gradientConicalAngle
                        onValueChanged: {
                            mbutton.stateUp.gradientConicalAngle = value
                        }
                    }
                    Label { text: "ConicalAngle" }
                }
                // RADIUS for radial gradient
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    visible: mbutton.stateUp.gradientType == GradientKind.TypeRadial
                    Slider {    
                        maxWidth: pageLUH.layoutFrame.width>>1
                        fromValue: 0.0
                        toValue: 2.0
                        value: mbutton.stateUp.gradientRadialRadius
                        onValueChanged: {
                            mbutton.stateUp.gradientRadialRadius = value
                        }
                    }
                    Label { text: "RadialRadius" }
                }
                Button {
                    text: "animate"
                    onClicked: {
                        itemAnimation.start()
                    }
                }
                
                
                attachedObjects: [
                    QPropertyAnimation {
                        id: itemAnimation
                        startValue: 0.0
                        endValue: 1.5
                        duration: 2500
                        easingCurve:  _app.easingCurve(QEasingCurve.OutElastic)
                        targetObject: mbutton.stateUp
                        propertyName: _app.qstringToQByteArray("roundPerc") // TODO:: workaround since we need the QByteArray of the property name
                        onFinished: {
                            console.log("ENDED")
                            mbutton.stateUp.roundPerc = 1;
                        }
                    }
                ]
                
                
                
                /////////////////////////////
                /////////////////////////////
                // CUSTOM BUTTON
                MBbutton {
                    id: mbutton
                    clickSound: true
                    onClicked: {
                        console.log("Clicked")
                    }
                    stateUp: MBcontainer {
                        shapeType: ShapeKind.TypeRoundedRect
                        fillerColor: Color.create(0, 0, 0.8, 0.6)
                        outlineColor: Color.create(0, 0, 0, 1)
                        roundPerc: 1
                        preferredWidth: 700
                        preferredHeight: 300
                        scalingMethod: ScalingMethod.AspectFill
                        gradientStops: [  
                            GradientStop { position: 1.0 ; color: Color.Blue } ,
                            GradientStop { position: 0.8 ; color: Color.Red } , 
                            GradientStop { position: 0.6 ; color: Color.Green } ,
                            GradientStop { position: 0.45; color: Color.Black } ,
                            GradientStop { position: 0.3 ; color: Color.create(0,0,0, .3) } ,
                            GradientStop { position: 0.2 ; color: Color.Cyan } , 
                            GradientStop { position: 0.0 ; color: Color.Yellow } 
                        ]
                    }
                    // if you want the same aspect as the unpressed button for the pressed state,
                    // reference the properties you would like to do not change to the stateUp and
                    // set the property that differs.
                    // In this example the fillerColor will change when pressed
                    stateDown: MBcontainer {
                        shapeType: mbutton.stateUp.shapeType
                        gradientType: mbutton.stateUp.gradientType
                        fillerColor: Color.create(0, 0, 0.3, 0.6)
                        outlineColor: Color.create(0.5, 0.5, 0.5, 1)
                        roundPerc: mbutton.stateUp.roundPerc
                        borderWidth: mbutton.stateUp.borderWidth
                        preferredWidth: mbutton.stateUp.preferredWidth
                        preferredHeight: mbutton.stateUp.preferredHeight
                        scalingMethod: mbutton.stateUp.scalingMethod
                        imageSource: mbutton.stateUp.imageSource
                        url: mbutton.stateUp.url
                        gradientConicalAngle: mbutton.stateUp.gradientConicalAngle
                        gradientRadialRadius: mbutton.stateUp.gradientRadialRadius
                        gradientStops: [  
                            GradientStop { position: 0.0 ; color: Color.Blue } ,
                            GradientStop { position: 0.2 ; color: Color.Red } , 
                            GradientStop { position: 0.3 ; color: Color.Green } ,
                            GradientStop { position: 0.45; color: Color.Black } ,
                            GradientStop { position: 0.6 ; color: Color.create(0,0,0, .3) } ,
                            GradientStop { position: 0.8 ; color: Color.Cyan } , 
                            GradientStop { position: 1.0 ; color: Color.Yellow } 
                        ]
                    }
                    // Here can be put the button icon, a label text or whatever else
                    // Mind that the layout of MBbutton is a DockLayout
//                    ImageView {
//                        imageSource: "asset:///images/ic_share.png"
//                        scalingMethod: ScalingMethod.AspectFit
//                        verticalAlignment: VerticalAlignment.Fill
//                        horizontalAlignment: HorizontalAlignment.Fill
//                    }
                }
            
            } // MBBUTTON PARAMETERS
        }
    

    
    
    
    }
}

/*
 * MBbutton.h
 *
 *  Created on: 12 Mar 2015
 *      Author: Marco Bavagnoli
 *      Copyright (c) 2015 Marco Bavagnoli <lil.deimos@gmail.com>
 */

/*
 * MBbutton is derived from MBcontainer
 * This Container defines its own DockLayout so it's not recommended to define one in QML
 *
 *  **** PROPERTIES ****
 * clickSound: plays the key input keypress system sound (true by default)
 * stateUp and stateDown properties define a MBcontainer for the unpressed and pressed states of the button
 *
 * QML example:
 * MBbutton {
 *      id: mbutton
 *      clickSound: true
 *      onClicked: {
 *          console.log("Clicked")
 *      }
 *      stateUp: MBcontainer {
 *                  shapeType: ShapeKind.TypeRoundedRect
 *                  roundPerc: 0.3
 *                  fillerColor: Color.create(0, 0, 0.8, 0.6)
 *                  outlineColor: Color.create(0.5, 0.5, 0.5, 1)
 *                  [...]
 *      }
 *      // if you want the same aspect as the unpressed button for the pressed state,
 *      // reference the properties you would like to do not change to the stateUp and
 *      // set the property that differs.
 *      // In this example the fillerColor will change when pressed
 *      stateDown: MBcontainer {
 *                  shapeType: mbutton.stateUp.shapeType
 *                  roundPerc: mbutton.stateUp.roundPerc
 *                  fillerColor: Color.create(0, 0, 0.3, 0.6)
 *                  outlineColor: mbutton.stateUp.outlineColor
 *                  [...]
 *      }
 * }
 */

#ifndef MBBUTTON_H_
#define MBBUTTON_H_

#include <QObject>
#include <bb/cascades/CustomControl>
#include <bb/cascades/Container>
#include <bb/cascades/TouchEvent>
#include <src/MBcontainer.h>

using namespace bb::cascades;
using namespace mbcontainer;

class MBbutton : public bb::cascades::Container
{
    Q_OBJECT

    Q_PROPERTY (mbcontainer::MBcontainer*    stateUp      READ stateUp      WRITE setStateUp      NOTIFY stateUpChanged)
    Q_PROPERTY (mbcontainer::MBcontainer*    stateDown    READ stateDown    WRITE setStateDown    NOTIFY stateDownChanged)
    Q_PROPERTY (bool            clickSound   READ clickSound   WRITE setClickSound   NOTIFY clickSoundChanged)

public:
    MBbutton();
    virtual ~MBbutton();

    mbcontainer::MBcontainer *stateUp() const { return m_stateUp; }
    void setStateUp(MBcontainer *mbc);

    mbcontainer::MBcontainer *stateDown() const { return m_stateDown; }
    void setStateDown(mbcontainer::MBcontainer *mbc);

    bool clickSound() const { return m_clickSound; }
    void setClickSound(bool s) { m_clickSound = s; emit clickSoundChanged(); }

private:
    mbcontainer::MBcontainer *m_stateUp;
    mbcontainer::MBcontainer *m_stateDown;
    bool m_clickSound;

    bool isCreated;

private Q_SLOTS:
    void onTouch(bb::cascades::TouchEvent* event);

Q_SIGNALS:
    void stateUpChanged();
    void stateDownChanged();
    void clickSoundChanged();

    void clicked();
};

#endif /* MBBUTTON_H_ */

/*
 * MBcontainer.h
 *
 *  Created on: 5 Mar 2015
 *      Author: Marco Bavagnoli <lil.deimos@gmail.com>
 *      Copyright (c) 2015 Marco Bavagnoli
 */

/*   MBcontainer is a custom control that can be used as a normal Container.
 *   It can have its custom color. The color can be a flat color, a linear, radial or conical gradient.
 *   Gradients can have as many stops as needed.
 *   MBcontainer can have a border with thickness and its own color.
 *   Different shapes are available. The default is TypeRoundedRect which can assube shapes from a rectangle
 *   to a circle/ellipses depending on the roundPerc property.
 *   An image can be drawn inscribed into the shape.
 *   Look here to understand how gradients work: http://qt-project.org/doc/qt-4.8/qgradient.html
 *
 *  **** PROPERTIES ****
 *  layoutHandler: this property is created at runtime and can be used as the LayoutUpdateHandler
 *  shapeType: possible values: ShapeKind.TypeRoundedRect (default)
 *                              ShapeKind.TypeBottomEdge
 *                              ShapeKind.TypeTopEdge
 *                              ShapeKind.TypeLeftEdge
 *                              ShapeKind.TypeRightEdge
 *                              ShapeKind.TypeSickleDown
 *                              ShapeKind.TypeSickleUp
 *                              ShapeKind.TypeSickleRight
 *                              ShapeKind.TypeSickleLeft
 *  roundPerc: expressed in 0-1 percentage. 0 menas hard edges, 1 means full rounded. Calculated on the minor edge.
 *  borderWidth: border thickness expressed in pixels
 *  outlineColor: border color (ex: Color.Yellow)
 *  fillerColor: filler color when the GradientKind.TypeNone is used
 *  gradientStops: GradientStop kind property. Is possible to define as many stops are needed. Example:
 *                 gradientStops: [
 *                      GradientStop { position: 1.0; color: Color.Blue } ,
 *                      GradientStop { position: 0.6; color: Color.Green } ,
 *                      GradientStop { position: 0.2; color: Color.Cyan } ,
 *                      GradientStop { position: 0.0; color: Color.Yellow }
 *                 ]
 *  gradientType: possible values:
 *                                GradientKind.TypeNone (default)
 *                                GradientKind.TypeLinear
 *                                GradientKind.TypeRadial
 *                                GradientKind.TypeConical
 *
 **** Properties used when the TypeLinear gradient is used. Values are expressed in 0-1 percentage ie top right corner has (0,0) coords, bottom right corner has (1,1)
 *  gradientStartX: x start position for the linear gradient expressed in 0-1 percentage
 *  gradientStartY: y start position for the linear gradient expressed in 0-1 percentage
 *  gradientStopX:  x stop position for the linear gradient expressed in 0-1 percentage
 *  gradientStopY:  y stop position for the linear gradient expressed in 0-1 percentage
 *
 **** Properties used when the TypeRadial gradient is used
 *  gradientRadialCenterX: x center position for the radial gradient expressed in 0-1 percentage
 *  gradientRadialCenterY; y center position for the radial gradient expressed in 0-1 percentage
 *  gradientRadialRadius: radius for the radial gradient expressed in 0-1 percentage (1.0 is the half of the minor edge)
 *
 **** Properties when the TypeConical gradient is used
 *  gradientConicalAngle: starting angle for the conical gradient brush. The angle must be specified in degrees between 0 and 360
 *
 *
 *  imageSource: local image file name
 *  scalingMethod: possible values:
 *                                  ScalingMethod.Fill
 *                                  ScalingMethod.AspectFit (default)
 *                                  ScalingMethod.AspectFill
 *
 *  url: remote image url
 *  imageScaleX: width scale. Must be >= 0 and 1 means no zoom
 *  imageScaleY: height scale. Must be >= 0 and 1 means no zoom
 */


#ifndef MBCONTAINER_H_
#define MBCONTAINER_H_

#include <QObject>
#include <QString>
#include <QtNetwork/qnetworkaccessmanager.h>
#include <QtNetwork/qnetworkreply.h>
#include <QtNetwork/qnetworkdiskcache.h>

#include <bb/cascades/CustomControl>
#include <bb/cascades/Container>
#include <bb/cascades/DockLayout>
#include <bb/cascades/Image>
#include <bb/cascades/ImageView>
#include <bb/cascades/LayoutUpdateHandler>
#include <bb/cascades/Color>
#include <bb/cascades/ScalingMethod>

using namespace bb::cascades;


namespace mbcontainer
{


class Q_DECL_EXPORT GradientKind
{
    Q_GADGET //  lighter-weight of Q_OBJECT setup
    Q_ENUMS(Type);

public:
    enum Type { TypeNone, TypeLinear, TypeRadial, TypeConical };

private:

    // Hide the constructor and destructor
    GradientKind();
    ~GradientKind();
};

class Q_DECL_EXPORT ShapeKind
{
    Q_GADGET //  lighter-weight of Q_OBJECT setup
    Q_ENUMS(Type);

public:
    enum Type { TypeRoundedRect, TypeBottomEdge, TypeTopEdge, TypeLeftEdge, TypeRightEdge, TypeSickleDown, TypeSickleUp, TypeSickleRight, TypeSickleLeft };

private:

    // Hide the constructor and destructor
    ShapeKind();
    ~ShapeKind();
};


class GradientStop : public QObject
{
    Q_OBJECT
    Q_PROPERTY (double               position  READ position  WRITE setPosition  NOTIFY positionChanged)
    Q_PROPERTY (bb::cascades::Color  color     READ color     WRITE setColor     NOTIFY colorChanged)

public:
    GradientStop(QObject *parent = 0) : QObject(parent) , m_position(0.0), m_color(bb::cascades::Color::DarkGreen) {};
    ~GradientStop() {};

    double position() {return m_position;}
    void   setPosition(double pos) { m_position = pos; emit positionChanged(); }

    bb::cascades::Color  color() { return m_color; }
    void   setColor(bb::cascades::Color c) { m_color = c; emit colorChanged(); }

private:
    double m_position;
    bb::cascades::Color  m_color;

Q_SIGNALS:
    void positionChanged();
    void colorChanged();
};


class MBcontainer : public bb::cascades::Container
{
    Q_OBJECT

    // Shape kind of this container
    Q_PROPERTY (int    shapeType     READ shapeType     WRITE setShapeType    NOTIFY shapeTypeChanged)

    // 0 means no round corner 1 means that the shorter edge is full-curved
    Q_PROPERTY (double roundPerc     READ roundPerc     WRITE setRoundPerc    NOTIFY roundPercChanged)
    // border width in pixels
    Q_PROPERTY (int    borderWidth   READ borderWidth   WRITE setBorderWidth  NOTIFY borderWidthChanged)
    // border color
    Q_PROPERTY (bb::cascades::Color  outlineColor  READ outlineColor  WRITE setOutlineColor  NOTIFY outlineColorChanged)
    // filler Container color
    Q_PROPERTY (bb::cascades::Color  fillerColor   READ fillerColor   WRITE setFillerColor   NOTIFY fillerColorChanged)

    // see how gradients works here:
    // http://qt-project.org/doc/qt-4.8/qgradient.html
    Q_PROPERTY (int    gradientType  READ gradientType  WRITE setGradientType  RESET unsetGradientType NOTIFY gradientTypeChanged)

    // Start and end coordinates to start compute the linear gradient. Expressed in percentage of width and eight Control, es: 0,0 Top-left corner   1,1 Bottom-right corner
    // Used only with TypeLinear
    Q_PROPERTY (double gradientStartX     READ gradientStartX   WRITE setGradientStartX    NOTIFY gradientStartXChanged)
    Q_PROPERTY (double gradientStartY     READ gradientStartY   WRITE setGradientStartY    NOTIFY gradientStartYChanged)
    Q_PROPERTY (double gradientStopX      READ gradientStopX    WRITE setGradientStopX     NOTIFY gradientStopXChanged)
    Q_PROPERTY (double gradientStopY      READ gradientStopY    WRITE setGradientStopY     NOTIFY gradientStopYChanged)

    // Center and radius of radial gradient (center also for conical). Expressed with 0-1 percentage. The 0-1 percentage of radius is the percentage of the smaller container edge
    Q_PROPERTY (double gradientRadialCenterX      READ gradientRadialCenterX    WRITE setGradientRadialCenterX     NOTIFY gradientRadialCenterXChanged)
    Q_PROPERTY (double gradientRadialCenterY      READ gradientRadialCenterY    WRITE setGradientRadialCenterY     NOTIFY gradientRadialCenterYChanged)
    Q_PROPERTY (double gradientRadialRadius       READ gradientRadialRadius     WRITE setGradientRadialRadius      NOTIFY gradientRadialRadiusChanged)

    // Angle for conical gradient
    Q_PROPERTY (double gradientConicalAngle       READ gradientConicalAngle     WRITE setGradientConicalAngle      NOTIFY gradientConicalAngleChanged)

    // If both a gradient and a color are specified, the gradient will be used
    // the gradient property should be set like:
    // gradientStops: [
    //    GradientStop { position: 1.0 ; color: Color.Blue } ,
    //    GradientStop { position: 0.6 ; color: Color.Green } ,
    //    GradientStop { position: 0.2 ; color: Color.Cyan } ,
    //    GradientStop { position: 0.0 ; color: Color.Yellow }
    // ]
    Q_PROPERTY (QDeclarativeListProperty<mbcontainer::GradientStop>          gradientStops         READ gradientStops    CONSTANT)

    Q_PROPERTY (QString imageSource   READ imageSource     WRITE setImageSource     RESET unsetImageSource   NOTIFY imageSourceChanged)
    Q_PROPERTY (QString url           READ url             WRITE setUrl             RESET unsetUrl           NOTIFY urlChanged)
    Q_PROPERTY (int   scalingMethod   READ scalingMethod   WRITE setScalingMethod                            NOTIFY scalingMethodChanged)
    Q_PROPERTY (double imageScaleX    READ imageScaleX     WRITE setImageScaleX     NOTIFY imageScaleXChanged)
    Q_PROPERTY (double imageScaleY    READ imageScaleY     WRITE setImageScaleY     NOTIFY imageScaleYChanged)

public:
    MBcontainer();
    virtual ~MBcontainer();

    // PROPERTIES
    int    shapeType() { return static_cast<int>(m_shapeType); }
    void   setShapeType(int type) {
        if (m_shapeType == static_cast<ShapeKind::Type>(type)) return;
        m_shapeType = static_cast<ShapeKind::Type>(type);
        init();
        emit shapeTypeChanged();
    }

    double roundPerc() { return m_roundPerc; }
    void   setRoundPerc(double r) { if (r<0.0 || m_roundPerc==r) return; m_roundPerc = r;  init(); emit roundPercChanged(); }

    int    borderWidth() { return m_borderWidth; }
    void   setBorderWidth(int b) { if (b<0 || m_borderWidth==b) return; m_borderWidth = b; init(); emit borderWidthChanged(); }

    bb::cascades::Color  outlineColor() { return m_outlineColor; }
    void   setOutlineColor(bb::cascades::Color c) { if (m_outlineColor==c) return; m_outlineColor = c; init(); emit outlineColorChanged(); }

    bb::cascades::Color  fillerColor() { return m_fillerColor; }
    void   setFillerColor(bb::cascades::Color c) { if (m_fillerColor==c) return; m_fillerColor = c; init(); emit fillerColorChanged(); }

    int    gradientType() { return static_cast<int>(m_gradientType); }
    void   setGradientType(int type) {
        if (m_gradientType == static_cast<GradientKind::Type>(type)) return;
        m_gradientType = static_cast<GradientKind::Type>(type);
        init();
        emit gradientTypeChanged();
    }
    void unsetGradientType() { m_gradientType=GradientKind::TypeNone; init(); emit gradientTypeChanged();}

    double gradientStartX() { return m_gradientStartX; }
    void   setGradientStartX(double x) { if (m_gradientStartX==x) return; m_gradientStartX = x; init(); emit gradientStartXChanged(); }

    double gradientStartY() { return m_gradientStartY; }
    void   setGradientStartY(double y) { if (m_gradientStartY==y) return; m_gradientStartY = y; init(); emit gradientStartYChanged(); }

    double gradientStopX() { return m_gradientStopX; }
    void   setGradientStopX(double x) { if (m_gradientStopX==x) return; m_gradientStopX = x; init(); emit gradientStopXChanged(); }

    double gradientStopY() { return m_gradientStopY; }
    void   setGradientStopY(double y) {  if (m_gradientStopY==y) return; m_gradientStopY = y; init(); emit gradientStopYChanged(); }

    double gradientRadialCenterX() { return m_gradientRadialCenterX; }
    void   setGradientRadialCenterX(double x) { if (m_gradientRadialCenterX==x) return; m_gradientRadialCenterX = x; init(); emit gradientRadialCenterXChanged(); }

    double gradientRadialCenterY() { return m_gradientRadialCenterY; }
    void   setGradientRadialCenterY(double y) { if (m_gradientRadialCenterY==y) return; m_gradientRadialCenterY = y; init(); emit gradientRadialCenterYChanged(); }

    double gradientRadialRadius() { return m_gradientRadialRadius; }
    void   setGradientRadialRadius(double r) { if (m_gradientRadialRadius==r || m_gradientRadialRadius<=0) return; m_gradientRadialRadius = r; init(); emit gradientRadialRadiusChanged(); }

    double gradientConicalAngle() { return m_gradientConicalAngle; }
    void   setGradientConicalAngle(double a) { if (m_gradientConicalAngle==a) return; m_gradientConicalAngle = a; init(); emit gradientConicalAngleChanged(); }

    QDeclarativeListProperty<mbcontainer::GradientStop> gradientStops()
    {
        return QDeclarativeListProperty<mbcontainer::GradientStop>(this, m_gradientStops);
    }
    int           gradientStopsCount() const { return m_gradientStops.count(); }
    mbcontainer::GradientStop *gradientStops(int id) const { return m_gradientStops.at(id); }

    QString imageSource() { return m_imageSource; }
    void    setImageSource(QString uri) { if (m_imageSource==uri) return; m_imageSource = uri; init(); emit imageSourceChanged(); }
    void    unsetImageSource() { m_imageSource = ""; init(); emit imageSourceChanged(); }

    QString url() { return m_url; }
    void    setUrl(QString url) { if (m_url==url) return; m_url = url; init(); emit urlChanged(); }
    void    unsetUrl() { m_url = ""; init(); emit urlChanged(); }

    int    scalingMethod() { return static_cast<int>(m_scalingMethod); }
    void   setScalingMethod(int sm) {
        if (m_scalingMethod==static_cast<bb::cascades::ScalingMethod::Type>(sm)) return;
        m_scalingMethod = static_cast<bb::cascades::ScalingMethod::Type>(sm);
        init();
        emit scalingMethodChanged();
    }

    double imageScaleX() { return m_imageScaleX; }
    void   setImageScaleX(double x) { if (x<0.0 || m_imageScaleX==x) return; m_imageScaleX = x;  init(); emit imageScaleXChanged(); }

    double imageScaleY() { return m_imageScaleY; }
    void   setImageScaleY(double y) { if (y<0.0 || m_imageScaleY==y) return; m_imageScaleY = y;  init(); emit imageScaleYChanged(); }


private:
    // PROPERTIES
    ShapeKind::Type m_shapeType;
    double m_roundPerc;
    int    m_borderWidth;
    bb::cascades::Color  m_outlineColor;
    bb::cascades::Color  m_fillerColor;
    GradientKind::Type  m_gradientType;
    double m_gradientStartX;
    double m_gradientStartY;
    double m_gradientStopX;
    double m_gradientStopY;
    double m_gradientRadialCenterX;
    double m_gradientRadialCenterY;
    double m_gradientRadialRadius;
    double m_gradientConicalAngle;
    QList<mbcontainer::GradientStop *>  m_gradientStops;
    QString m_imageSource;
    QString m_url;
    bb::cascades::ScalingMethod::Type m_scalingMethod;
    double m_imageScaleX;
    double m_imageScaleY;

    static QNetworkAccessManager * m_netManager;
    static QNetworkDiskCache * m_networkDiskCache;
    QNetworkReply            *m_reply;
    QPainterPath             m_path;
    QLinearGradient          m_linearGradient;
    QRadialGradient          m_radialGradient;
    QConicalGradient         m_conicalGradient;
    QBrush                   m_fillerBrush;
    QImage                   m_imgData;

    bb::cascades::ImageView  *m_backgroundImg;
    LayoutUpdateHandler      *m_luh;

    bool isCreated;

    void computeFillerColor();
    void computeShape();
    void init();
    void inscribeImage();
    void drawContainer();

public Q_SLOTS:
    void onCreationCompleted();
    void onResChanged(float r);
    void imageLoaded();
    void onError(QNetworkReply::NetworkError);

Q_SIGNALS:
    void shapeTypeChanged();
    void roundPercChanged();
    void borderWidthChanged();
    void outlineColorChanged();
    void fillerColorChanged();
    void gradientTypeChanged();
    void gradientStartXChanged();
    void gradientStartYChanged();
    void gradientStopXChanged();
    void gradientStopYChanged();
    void gradientRadialCenterXChanged();
    void gradientRadialCenterYChanged();
    void gradientRadialRadiusChanged();
    void gradientConicalAngleChanged();
    void imageSourceChanged();
    void urlChanged();
    void scalingMethodChanged();
    void imageScaleXChanged();
    void imageScaleYChanged();

    void errorMsg(QString error);
    void imageDownloadStarted();
    void imageDownloadError();
    void imageDownloaded();
};

} // end namespace

#endif /* MBCONTAINER_H_ */

/*
 * MBbutton.cpp
 *
 *  Created on: 12 Mar 2015
 *      Author: Marco Bavagnoli
 *      Copyright (c) 2015 Marco Bavagnoli <lil.deimos@gmail.com>
 */

#include <bb/cascades/DockLayout>
#include <bps/soundplayer.h>

#include <src/MBbutton.h>

MBbutton::MBbutton() :
    m_stateUp(NULL),
    m_stateDown(NULL),
    m_clickSound(true)
{
    isCreated = false;

    this->setLayout(DockLayout::create());

    bool b;
    b = QObject::connect(this, SIGNAL(touch(bb::cascades::TouchEvent*)) , this,  SLOT(onTouch(bb::cascades::TouchEvent*)) );
    Q_ASSERT(b);
}

MBbutton::~MBbutton()
{
}

void MBbutton::onTouch(bb::cascades::TouchEvent* event)
{
    static bool pressed = false;
    if (event->isDown()) {
        pressed = true;
        if (m_stateDown!=NULL) {
            m_stateUp->setVisible(false);
            m_stateDown->setVisible(true);
        }
    }
    if ( (event->isUp() || event->isCancel())  && pressed) {
        if (m_stateDown!=NULL) {
            m_stateUp->setVisible(true);
            m_stateDown->setVisible(false);
        }
        if (event->isCancel()) pressed = false;
    }
    if (event->isUp() && pressed) {
        pressed = false;
        emit clicked();
        if (m_clickSound) soundplayer_play_sound("input_keypress");
    }
}


void MBbutton::setStateUp(MBcontainer *mbc)
{
    m_stateUp = mbc;
    this->add(mbc);
    emit stateUpChanged();
}

void MBbutton::setStateDown(MBcontainer *mbc)
{
    m_stateDown = mbc;
    m_stateDown->setVisible(false);
    this->add(mbc);
    emit stateDownChanged();
}


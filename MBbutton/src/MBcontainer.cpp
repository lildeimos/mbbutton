/*
 * MBcontainer.cpp
 *
 *  Created on: 5 Mar 2015
 *      Author: Marco Bavagnoli <lil.deimos@gmail.com>
 *      Copyright (c) 2015 Marco Bavagnoli
 */

#include <src/MBcontainer.h>
#include <qt4/QtGui/qimage.h>
#include <qt4/QtGui/qpainter.h>
#include <QtNetwork/qnetworkreply.h>
#include <QtNetwork/qnetworkdiskcache.h>

using namespace mbcontainer;

QNetworkAccessManager * MBcontainer::m_netManager = new QNetworkAccessManager();
QNetworkDiskCache * MBcontainer::m_networkDiskCache = new QNetworkDiskCache();

MBcontainer::MBcontainer() :
    m_shapeType(ShapeKind::TypeRoundedRect),
    m_roundPerc(0.8),
    m_borderWidth(4),
    m_outlineColor(Color::DarkGray),
    m_fillerColor(Color::fromRGBA(1.0f, 1.0f, 1.0f, 0.7f)),
    m_gradientType(GradientKind::TypeNone),
    m_gradientStartX(0.5),
    m_gradientStartY(0.0),
    m_gradientStopX(0.5),
    m_gradientStopY(1.0),
    m_gradientRadialCenterX(0.5),
    m_gradientRadialCenterY(0.5),
    m_gradientRadialRadius(0.5),
    m_gradientConicalAngle(0.0),
    m_imageSource(QString()),
    m_url(QString()),
    m_scalingMethod(ScalingMethod::AspectFit),
    m_imageScaleX(1.0),
    m_imageScaleY(1.0),
    m_backgroundImg(NULL),
    m_luh(NULL),
    m_reply(NULL)
{
    isCreated = false;

    // add a layoutHandler property to this CustomControl to retrieve its dimensions
    m_luh = new LayoutUpdateHandler(this);
    QVariant v = QVariant::fromValue(m_luh);
    this->setProperty("layoutHandler", v );

    bool b;
    b = QObject::connect(this, SIGNAL(preferredHeightChanged(float)), this, SLOT(onResChanged(float)));
    Q_ASSERT(b);
    b = QObject::connect(this, SIGNAL(preferredWidthChanged(float)), this, SLOT(onResChanged(float)));
    Q_ASSERT(b);
    b = QObject::connect(this, SIGNAL(creationCompleted()), this, SLOT(onCreationCompleted()));
    Q_ASSERT(b);

    this->setLayout(DockLayout::create());

    // Set manager cache. The cache directory is in the app/tmp/
    m_networkDiskCache->setCacheDirectory(QDir::currentPath()+"/tmp");
    m_netManager->setCache(m_networkDiskCache);
}

MBcontainer::~MBcontainer()
{
}

void MBcontainer::onResChanged(float r)
{
    Q_UNUSED(r);
    init();
}

void MBcontainer::onCreationCompleted()
{
    isCreated = true;
    init();
}

void MBcontainer::computeFillerColor()
{
    if (m_gradientType == GradientKind::TypeNone || m_gradientStops.isEmpty()) {
        m_fillerBrush = QBrush(QColor(m_fillerColor.blue()*255,m_fillerColor.green()*255,m_fillerColor.red()*255, m_fillerColor.alpha()*255 ));
        return;
    }
    QGradient *grad;
    if (m_gradientType == GradientKind::TypeLinear) {
        m_linearGradient.setStart(this->preferredWidth() * m_gradientStartX, this->preferredHeight() * m_gradientStartY);
        m_linearGradient.setFinalStop(this->preferredWidth() * m_gradientStopX, this->preferredHeight() * m_gradientStopY);
        grad = &m_linearGradient;
        m_fillerBrush = QBrush(m_linearGradient);
    }
    if (m_gradientType == GradientKind::TypeRadial) {
        m_radialGradient.setCenter(this->preferredWidth() * m_gradientRadialCenterX, this->preferredHeight() * m_gradientRadialCenterY);
        m_radialGradient.setFocalPoint(this->preferredWidth() * m_gradientRadialCenterX, this->preferredHeight() * m_gradientRadialCenterY);
        m_radialGradient.setRadius( qMin(this->preferredWidth(), this->preferredHeight()) * m_gradientRadialRadius );
        grad = &m_radialGradient;
        m_fillerBrush = QBrush(m_radialGradient);
    }
    if (m_gradientType == GradientKind::TypeConical) {
        m_conicalGradient.setCenter(this->preferredWidth() * m_gradientRadialCenterX, this->preferredHeight() * m_gradientRadialCenterY);
        m_conicalGradient.setAngle(m_gradientConicalAngle);
        grad = &m_conicalGradient;
        m_fillerBrush = QBrush(m_conicalGradient);
    }

    // Set stops whatever gradient type is choosen
    for (int i=0; i<m_gradientStops.count(); i++) {
        Color bbColor;
        bbColor = gradientStops(i)->color();
        QColor c(bbColor.blue()*255,  bbColor.green()*255,  bbColor.red()*255,   bbColor.alpha()*255);
        grad->setColorAt(gradientStops(i)->position(), c);
    }

    switch ( m_gradientType ) {
        case GradientKind::TypeNone:    m_fillerBrush = QBrush(QColor(m_fillerColor.blue()*255,m_fillerColor.green()*255,m_fillerColor.red()*255, m_fillerColor.alpha()*255 )); break;
        case GradientKind::TypeLinear:  m_fillerBrush = QBrush(m_linearGradient);  break;
        case GradientKind::TypeRadial:  m_fillerBrush = QBrush(m_radialGradient);  break;
        case GradientKind::TypeConical: m_fillerBrush = QBrush(m_conicalGradient); break;
    }
}

void MBcontainer::computeShape()
{
    int w = this->preferredWidth();
    int h = this->preferredHeight();
    m_path = QPainterPath();
    double radiusPerc = 0.5 * qMin(w, h) * roundPerc();

    int x = m_borderWidth>>1;
    int y = m_borderWidth>>1;
    w -= m_borderWidth;
    h -= m_borderWidth;

    switch ( m_shapeType ) {
        default:
        case ShapeKind::TypeRoundedRect:
             m_path.addRoundedRect ( x, y,
                                     w, h,
                                     radiusPerc, radiusPerc, Qt::AbsoluteSize );
             break;
        case ShapeKind::TypeBottomEdge:
             if (radiusPerc>qMin(w, h)*0.5) radiusPerc = qMin(w, h)*0.5;
             m_path.moveTo(x,h);
             m_path.lineTo(w,h);
             m_path.quadTo(w,y,   w-radiusPerc,y);
             m_path.lineTo(radiusPerc,y);
             m_path.quadTo(x,y,  x,h);
             break;
        case ShapeKind::TypeTopEdge:
             if (radiusPerc>qMin(w, h)*0.5) radiusPerc = qMin(w, h)*0.5;
             m_path.moveTo(x,y);
             m_path.lineTo(w,y);
             m_path.quadTo(w,h,  w-radiusPerc,h);
             m_path.lineTo(radiusPerc,h);
             m_path.quadTo(x,h,  x,y);
             break;
        case ShapeKind::TypeLeftEdge:
             if (radiusPerc>qMin(w, h)*0.5) radiusPerc = qMin(w, h)*0.5;
             m_path.moveTo(x,y);
             m_path.lineTo(x,h);
             m_path.quadTo(w,h,  w,h-radiusPerc);
             m_path.lineTo(w,radiusPerc);
             m_path.quadTo(w,y,  x,y);
             break;
        case ShapeKind::TypeRightEdge:
             if (radiusPerc>qMin(w, h)*0.5) radiusPerc = qMin(w, h)*0.5;
             m_path.moveTo(w,y);
             m_path.lineTo(w,h);
             m_path.quadTo(x,h,  x,h-radiusPerc);
             m_path.lineTo(x,radiusPerc);
             m_path.quadTo(x,y,  w,y);
             break;
        case ShapeKind::TypeSickleDown:
             m_path.moveTo(x,y);
             m_path.quadTo(w>>1,h>>1,  w,y);
             m_path.cubicTo(w,h>>1, (w>>1)+radiusPerc,h,    w>>1,h);
             m_path.cubicTo((w>>1)-radiusPerc,h,  x,h>>1,   x,y);
             break;
        case ShapeKind::TypeSickleUp:
             m_path.moveTo(x,h);
             m_path.quadTo(w>>1,h>>1,  w,h);
             m_path.cubicTo(w,h>>1, (w>>1)+radiusPerc,y,    w>>1,y);
             m_path.cubicTo((w>>1)-radiusPerc,y,  x,h>>1,   x,h);
             break;
        case ShapeKind::TypeSickleRight:
             m_path.moveTo(x,y);
             m_path.quadTo(w>>1,h>>1,  x,h);
             m_path.cubicTo(w>>1,h, w,(h>>1)+radiusPerc,    w,h>>1);
             m_path.cubicTo(w,(h>>1)-radiusPerc,  w>>1,y,   x,y);
             break;
        case ShapeKind::TypeSickleLeft:
             m_path.moveTo(w,y);
             m_path.quadTo(w>>1,h>>1,  w,h);
             m_path.cubicTo(w>>1,h, x,(h>>1)+radiusPerc,    x,h>>1);
             m_path.cubicTo(x,(h>>1)-radiusPerc,  w>>1,y,   w,y);
             break;
    }
}


////////////////////////////////////////////////////
// when this changes res the button must be repainted
void MBcontainer::init()
{
    if (!isCreated || !this->isPreferredHeightSet() || !this->isPreferredWidthSet() ||
         this->preferredWidth()==0 || this->preferredHeight()==0   ) return;

    computeShape();

    computeFillerColor();

    if (!m_imageSource.isEmpty()) {
        QString imageSource;
        if (m_imageSource.startsWith("asset")) {
            imageSource = QDir::currentPath() + "/app/native/assets" + m_imageSource.right(m_imageSource.length()-QString("asset://").length());
        }
        if (m_imageSource.startsWith("file:")) {
            imageSource = m_imageSource.right(m_imageSource.length()-QString("file://").length());
        }

        if (!m_imgData.load(imageSource)) return;
        inscribeImage();
    } else
        m_imgData = QImage();

    // get image from url if defined and a local image is not used
    if (!m_url.isEmpty() && m_imageSource.isEmpty()) {
        QNetworkRequest request;
        request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
        request.setUrl(m_url);

        m_reply = m_netManager->get(request);
        bool b = QObject::connect(m_reply, SIGNAL(finished()), this, SLOT(imageLoaded()));
        Q_ASSERT(b);
        b = QObject::connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(onError(QNetworkReply::NetworkError)));
        Q_ASSERT(b);
        emit imageDownloadStarted();
    } else
        drawContainer();
}

void MBcontainer::onError(QNetworkReply::NetworkError error)
{
    Q_UNUSED(error);
    m_reply->deleteLater();
    m_reply = NULL;
    emit imageDownloadError();
}

void MBcontainer::imageLoaded()
{
    if (m_reply == NULL) {
        m_reply->deleteLater();
        return;
    }
    // It's possible that with no error a forwarded link is been returned
    QString forwardedUrl = m_reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl().toString();
    if (!forwardedUrl.isEmpty()) {
        QNetworkRequest request;
        request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
        request.setUrl(forwardedUrl);

        m_netManager->get(request);
        return;
    }
    QByteArray data = m_reply->readAll();
    m_reply->deleteLater();
    m_reply = NULL;
    if (!m_imgData.loadFromData(data))
        return;

    inscribeImage();
    drawContainer();
    emit imageDownloaded();
}

// here m_imgData is alreaddy set. Put it in the path with the right aspect ratio
void MBcontainer::inscribeImage()
{
    QImage imgToLoad(QSize(this->preferredWidth(),this->preferredHeight()), QImage::Format_ARGB32_Premultiplied);
    Qt::AspectRatioMode ar;
    switch (m_scalingMethod) {
        default:
        case ScalingMethod::AspectFit:  ar = Qt::KeepAspectRatio; break;
        case ScalingMethod::AspectFill: ar = Qt::KeepAspectRatioByExpanding; break;
        case ScalingMethod::Fill:       ar = Qt::IgnoreAspectRatio; break;
    }
    m_imgData = m_imgData.scaled(this->preferredWidth()*m_imageScaleX, this->preferredHeight()*m_imageScaleY, ar );
    // swap red and blu !
    uchar *ptr = m_imgData.bits();
    uchar tmp;
    for (int i=0; i<m_imgData.byteCount() - 4; i+=4) {
        tmp = ptr[i+0];
        ptr[i+0] = ptr[i+2];
        ptr[i+2] = tmp;
    }

    // inscribe the loaded image into the size of container
    int dx = (imgToLoad.width() - m_imgData.width()) >> 1;
    int dy = (imgToLoad.height()- m_imgData.height()) >> 1;
    QPainter painter;
    painter.begin(&imgToLoad);
    painter.fillPath( m_path, m_fillerBrush );
    painter.drawImage(QPoint(dx,dy), m_imgData);
    painter.end();
    m_imgData = imgToLoad;
}

void MBcontainer::drawContainer()
{
    QImage img(QSize(this->preferredWidth(),this->preferredHeight()), QImage::Format_ARGB32_Premultiplied);
    memset(img.bits(), 0x00, img.byteCount());

    QPen   pen;
    // Bah Boh eBhe.... hum red and blu swapped ?!?!?`#@ł#@][@« æßðđð
    pen.setColor( QColor(m_outlineColor.blue()*255,m_outlineColor.green()*255,m_outlineColor.red()*255, m_outlineColor.alpha()*255) );
    pen.setWidth(m_borderWidth);
    pen.setJoinStyle(Qt::MiterJoin);


    // Draw the background image. A white filled path if the imageSource is set
    QPainter painter;
    painter.begin(&img);
    painter.setRenderHint( QPainter::HighQualityAntialiasing, true );
    // if there is no image laded fill the path else draw a filler path for the mask
    if (m_imgData.isNull()) {
        painter.fillPath( m_path, m_fillerBrush );
        if (m_borderWidth!=0) {
            painter.setPen(pen);
            painter.drawPath(m_path);
        }
    } else
    {
        painter.fillPath( m_path, QBrush(QColor(255,255,255, 255 )) );
        painter.setPen(QPen(QColor(255,255,255, 255)));
        painter.drawPath(m_path);
    }
    painter.end();




    if (!m_imgData.isNull()) {
        // Set the transparency value based on the filled path drawn before
        quint32 *c1 = (quint32*)m_imgData.bits();
        quint32 *c2 = (quint32*)img.bits();
        for (int i=0; i<img.byteCount()>>2; i++) // 4 is alpha byte
        {
            if (!(c2[i] & 0xff)) c1[i] = 0x00;
        }

        // Now draw the border
        painter.begin(&m_imgData);
        painter.setRenderHint( QPainter::HighQualityAntialiasing, true );
        if (m_borderWidth!=0) {
            painter.setPen(pen);
            painter.drawPath(m_path);
        }
        painter.end();
        img = m_imgData;
    }




    if (m_backgroundImg == NULL) {
        m_backgroundImg = new ImageView();
    }
    m_backgroundImg->resetImage();

    bb::ImageData imageData(bb::PixelFormat::RGBA_Premultiplied, img.width(), img.height());
    if (!imageData.isValid()) {
        qDebug() << "error allocating ImageData";
        emit errorMsg("error allocating ImageData");
        return;
    }
    unsigned char *dst = imageData.pixels();
    memcpy(dst, img.bits(), img.byteCount());

    Image bbImg(imageData);
    m_backgroundImg->setImage(bbImg);

    this->insert(0, m_backgroundImg);
}



